<?php

	/*

		CREDIT
		---------------------------------------
		Author: 	Martin Hlavacka
		Contact: 	martinhlavacka@outlook.com 
		Date: 		06.04.2018
				
		LICENSE
		---------------------------------------
		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in all
		copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
		SOFTWARE.

		USAGE
		---------------------------------------
		1. Create a free account at https://onesignal.com/
		2. Update values below
		3. Updte API token in jQuery (at the bottom)

	*/

	// PLUGIN SETTINGS
	$push_language	= "en";
	$push_auth  	= "REST API KEY";
	$push_app_id 	= "API-TOKEN";
	$push_img		= "LINK-TO-IMAGE";

	// PUSH VARIABLES
	$push_title 	= "PUSH-TITLE";
	$push_message 	= "DISPLAYED_MESSAGE";
	$push_url		= "SEND-TO-LINK";

	// CREATE NEW OBJECT
	$push = new Push($push_language, $push_auth, $push_app_id, $push_img, $push_title, $push_message, $push_url);
	
	// CLASS DEFINITION
	class Push{

		// PROPERTIES
		private $push_prop_language;
		private $push_prop_auth;
		private $push_prop_app_id;
		private $push_prop_img;
		private $push_prop_title;
		private $push_prop_message;
		private $push_prop_url;
		private $push_prop_heading;
		private $push_prop_content;
		private $push_prop_fields;
		private $push_prop_ch;
		private $push_prop_response;

		// CONSTRUCTOR
		public function __construct($push_language, $push_auth, $push_app_id, $push_img, $push_title, $push_message, $push_url){

			// BIND PARAMETERS
			$this->push_prop_language 	= $push_language;
			$this->push_prop_auth	 	= $push_auth;
			$this->push_prop_app_id 	= $push_app_id;
			$this->push_prop_img		= $push_img;
			$this->push_prop_title 		= $push_title;
			$this->push_prop_message 	= $push_message;
			$this->push_prop_url 		= $push_url;
			$this->push_prop_heading	= "";
			$this->push_prop_content	= "";
			$this->push_prop_fields		= "";
			$this->push_prop_ch			= "";
			$this->push_prop_response	= "";

			// PREPARE PUSH
			$this->preparePush();

		}

		// METHOD PREPARING NOTIFICAION
		public function preparePush(){

			// MESSAGE CONTENT
			$this->push_prop_heading = array($this->push_prop_language => $this->push_prop_title);
			$this->push_prop_content = array($this->push_prop_language => $this->push_prop_message);

			// SETTINGS
			$this->push_prop_fields = array(
				'app_id' 			=> $this->push_prop_app_id,
				'included_segments' => array('All'),
				'data'				=> array("foo" => "bar"),
				'url'				=> $this->push_prop_url,
				'headings'			=> $this->push_prop_heading,
				'contents' 			=> $this->push_prop_content,
				'chrome_web_icon' 	=> $this->push_prop_img,
				'small_icon'		=> $this->push_prop_img,
				'large_icon'		=> $this->push_prop_img
			);

			// SEND NOTIFICATION
			$this->sendPush();
		}

		// METHOD SENDINF NOTIFICATION
		public function sendPush(){
			
			// API CALL
			$this->push_prop_fields = json_encode($this->push_prop_fields);
			$this->push_prop_ch = curl_init();
			curl_setopt($this->push_prop_ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
			curl_setopt($this->push_prop_ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Authorization: Basic ' . $this->push_prop_auth));
			curl_setopt($this->push_prop_ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($this->push_prop_ch, CURLOPT_HEADER, FALSE);
			curl_setopt($this->push_prop_ch, CURLOPT_POST, TRUE);
			curl_setopt($this->push_prop_ch, CURLOPT_POSTFIELDS, $this->push_prop_fields);
			curl_setopt($this->push_prop_ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			$this->push_prop_response = curl_exec($this->push_prop_ch);
			curl_close($this->push_prop_ch);
			
			// DISPLAY RESULT
			print_r($this->push_prop_response);

		}

	}

?>

<!-- IMPORT SCRIPT -->
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>

<!-- EXECUTE SCRIPT -->
<script>
	var OneSignal = window.OneSignal || [];
	OneSignal.push(function() {
		OneSignal.init({
	      	appId: "API-TOKEN",
	    });
	});
</script>
